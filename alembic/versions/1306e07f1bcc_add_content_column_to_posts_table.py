"""add content column to posts table

Revision ID: 1306e07f1bcc
Revises: 54e7bdf6ead2
Create Date: 2023-11-22 00:06:04.590624

"""
from typing import Sequence, Union

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision: str = '1306e07f1bcc'
down_revision: Union[str, None] = '54e7bdf6ead2'
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:
    op.add_column("posts", sa.Column("content", sa.String(), nullable=False))
    pass


def downgrade() -> None:
    op.drop_column("posts", "content")
    pass
