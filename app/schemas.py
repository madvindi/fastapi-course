from datetime import datetime
from pydantic import BaseModel, ConfigDict, EmailStr
from pydantic.types import conint
from typing import Optional

class UserCreate(BaseModel):
    email: EmailStr
    password: str
    
class UserOut(BaseModel):
    id: int
    email: EmailStr
    created_at: datetime
    
    class Config:
        orm_mode = True 
        
class UserLogin(BaseModel):
    email: EmailStr
    password: str

class PostBase(BaseModel):
    title: str
    content: str
    # rating: Optional[int] = None # similar to Rust's Option<> i.e Some or none
    published: bool = True # acts as a default value
# req schema
class PostCreate(PostBase):
    pass 

# res schema
class Post(PostBase):
    id: int
    created_at: datetime
    owner_id: int
    owner: UserOut

    class Config:
        orm_mode = True
        
class PostOut(BaseModel):
    Post: Post
    votes: int
    
    class Config:
        orm_mode = True
  
class Token(BaseModel):
    access_token: str
    token_type: str

class TokenData(BaseModel):
    model_config = ConfigDict(coerce_numbers_to_str=True) # if you skip this pydantic will throw an error
    id: Optional[str] = None
    
class Vote(BaseModel):
    post_id: int
    dir: conint(le=1) # type: ignore
 