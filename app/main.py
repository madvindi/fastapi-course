from fastapi import FastAPI
import fastapi
from . import models
from .database import engine
from .routers import post, user, auth, vote
from fastapi.middleware.cors import CORSMiddleware

# we don't need this since we are using alembic now.
# models.Base.metadata.create_all(bind=engine)  (this was used to generate the table when the app started first)

app = FastAPI()

#origins = ["https://www.google.com", "https://www.youtube.com"]
origins = ["*"]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(auth.router)
app.include_router(post.router)
app.include_router(user.router)
app.include_router(vote.router)

@app.get("/")
async def root():
    return {"message": "hello world!!!, Please visit /docs route for the documentation of this API"}

# my_posts = [
#     {"id": 1, "title": "Mock Title 1", "content": "Mock Content 1"},
#     {"id": 2, "title": "Mock Title 2", "content": "Mock Content 2"},
#     {"id": 3, "title": "Mock Title 3", "content": "Mock Content 3"},
#     # Add more dictionaries as needed
# ]

# def find_post(id):
#     for p in my_posts:
#         if p["id"]==id:
#             return p

# def find_index_post(id):
#     for i, p in enumerate(my_posts):
#         if p['id']==id: 
#             return i