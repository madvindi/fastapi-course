from pydantic_settings import BaseSettings, SettingsConfigDict

#from pydantic import BaseSettings

class Settings(BaseSettings): # type: ignore
    database_hostname: str
    database_port: str
    database_password: str
    database_name: str
    database_username: str
    secret_key: str
    algorithm: str
    access_token_expire_minutes: int
    
    # _env_file: str = "../.env"
    
    model_config = SettingsConfigDict(env_file=".env")
    
settings = Settings() # type: ignore